# Build a REST API with Fastify & Prisma

Video: https://youtu.be/LMoMHP44-xM

## What are we using?
* Fastify - Web server
* Prisma - Database ORM
* Zod - Request and response validation
* TypeScript

## Features
* Create a user
* Login
* List users
* Create a product
* List products
* Authentication
* Request & response validation

## Installation
Run the following commands:

```
docker-compose -f postgres/docker-compose.yml

npm install
npx prisma init --datasource-provider postgres
npx prisma migrate dev --name init
node run dev
```

Then you can use test.http file to test the application.