import { hashPassword } from "../../src/utils/hash";
import prisma from "../../src/utils/prisma";
import { CreateUserInput } from "../../schemas/schemas";

export async function createUser(input: CreateUserInput) {
  const { password, ...rest } = input;

  const { hash, salt } = hashPassword(password);

  const user = await prisma.user.create({
    data: { ...rest, salt, password: hash },
  });

  return user;
}

export async function findUserByEmail(email: string) {
  return prisma.user.findUnique({
    where: {
      email,
    }
  })
}

export async function findUsers() {
  return prisma.user.findMany({
    select: {     // select the field to return in prisma
      email: true,
      name: true,
      id: true,
    }
  });
}