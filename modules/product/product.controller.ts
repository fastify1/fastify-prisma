import { FastifyReply, FastifyRequest } from "fastify";
import { createProduct, getMyProducts, getProductById, getProducts } from "./product.service";
import { CreateProductInput, GetProductInput } from "./product.schema"; 
import { server } from "../../src/app";

export async function createProductHandler(request: FastifyRequest<{Body: CreateProductInput}>) {
  const product = await createProduct({
    ...request.body,
    ownerId: request.user.id,
  });

  return product;
}

export async function getProductsHandler() {
  const products = await getProducts();

  return products;
}

export async function getMyProductsHandler (request: FastifyRequest, reply: FastifyReply) {
  const token = request.headers.authorization?.split(' ')[1];

  if(!token) {
    return reply.code(401).send({message: 'Unauthorized'});
  }

  const { id } = server.jwt.decode(token) as { id: number, email: string, name: string, iat: number };
  const myProducts = getMyProducts(id);

  return myProducts;
}

export async function getProductByIdHandler (request: FastifyRequest<{Params: GetProductInput}>, reply: FastifyReply) {
  const { id } = request.params;

  const product = await getProductById(Number(id));
  return product;
}