import { z } from "zod";
import { buildJsonSchemas } from 'fastify-zod';
import { userCore } from "../user/user.schema";

const productInput = {
  title: z.string(),
  price: z.number(),
  content: z.string().optional(),
};

const productGenerated = {
  id: z.number(),
  createdAt: z.string(),
  updatedAt: z.string(),
};

const createProductSchema = z.object({
  ...productInput
});

const getProductSchema = z.object({
  id: z.number(),
})

const productFullResponseSchema = z.object({
  ...productInput,
  ...productGenerated,
  owner: z.object({
    ...userCore,
    id: z.number(),
  }),
});

const productShortResponseSchema = z.object({
  ...productInput,
  ...productGenerated,
});

const productsFullResponseSchema = z.array(productFullResponseSchema);

const productsShortResponseSchema = z.array(productShortResponseSchema);

export type CreateProductInput = z.infer<typeof createProductSchema>;

export type GetProductInput = z.infer<typeof getProductSchema>;

export const { schemas: productSchemas, $ref } = buildJsonSchemas({
  createProductSchema,
  getProductSchema,
  productFullResponseSchema,
  productShortResponseSchema,
  productsFullResponseSchema,
  productsShortResponseSchema
}, {
  $id: 'productSchemas'
})