import prisma from "../../src/utils/prisma";
import { CreateProductInput } from "../../schemas/schemas";

export async function createProduct(data: CreateProductInput & {ownerId: number}) {
  return prisma.product.create({
    data,
  })
}

export async function getProducts() {
  return prisma.product.findMany({
    select: {
      content: true,
      createdAt: true,
      updatedAt: true,
      title: true,
      price: true,
      id: true,
      owner: {
        select: {
          name: true,
          id: true,
          email: true,
        }
      }
    }
  })
}

export async function getMyProducts(id: number) {
  return prisma.product.findMany({
    select: {
      content: true,
      createdAt: true,
      updatedAt: true,
      title: true,
      price: true,
      id: true,
      owner: {
        select: {
          name: true,
          id: true,
          email: true,
        }
      }
    },
    where: {
      ownerId: id,
    }
  })
}

export async function getProductById(id: number) {
  return prisma.product.findUnique({
    where:{
      id
    }
  })
}