import { FastifyInstance } from "fastify";
import { createProductHandler, getProductsHandler, getMyProductsHandler, getProductByIdHandler } from "./product.controller";
import { $ref } from "./product.schema";

async function productRoutes(server: FastifyInstance) {

  server.post('/', {
    preHandler: [server.authenticate],
    schema: {
      body: $ref('createProductSchema'),
      response: {
        201: $ref('productFullResponseSchema')
      }
    }
  }, createProductHandler);

  server.get('/', {
    schema: {
      response: {
        200: $ref('productsShortResponseSchema'),
      }
    }
  }, getProductsHandler);

  server.get('/myproducts', {
    preHandler: [server.authenticate],
    schema: {
      response: {
        200: $ref('productsFullResponseSchema')
      }
    }
  }, getMyProductsHandler);

  server.get('/:id', {
    schema: {
      response: {
        200: $ref('productShortResponseSchema')
      }
    }
  }, getProductByIdHandler)
}

export default productRoutes;