import 'dotenv/config'
import Fastify, { FastifyReply, FastifyRequest } from "fastify";
import fastifyJwt from '@fastify/jwt'
import userRoutes from "../modules/user/user.route";
import { userSchemas } from '../modules/user/user.schema'; 
import { productSchemas } from '../modules/product/product.schema';
import productRoutes from "../modules/product/product.route";

export const server = Fastify({logger: true});

server.register(fastifyJwt, { //his will decorate your fastify instance with the following methods: decode, sign, and verify; request.jwtVerify and reply.jwtSign
  secret: process.env.ACCESS_TOKEN_SECRET as string
});

server.decorate('authenticate', async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    await request.jwtVerify();
  } catch(e) {
    return reply.send(e)
  }
})

server.get('/healthcheck', async function() {
  return { status: "OK"};
});

async function main() {

  for (const schema of [...userSchemas, ...productSchemas]) {
    server.addSchema(schema);
  }

  server.register(userRoutes, {prefix: 'api/users'});
  server.register(productRoutes, {prefix: 'api/products'});

  try {

    await server.listen({ port: 3000, host: '127.0.0.1' }); //only use 0.0.0.0 when deploying with Docker, because listening on all interfaces comes with inherent security risks.

    console.log('Server ready at http://localhost:3000');

  } catch(e) {
    console.error(e);
    process.exit(1);
  }
}

main();